const temperaturespan=document.getElementById('temperature')
const latitude=document.getElementById('latitude')
const longitude=document.getElementById('longitude')
const trivandrum=document.getElementById('trivandrum')
const kottayam=document.getElementById('kottayam')
const currentlocation=document.getElementById('currentlocation')
const weatherform=document.getElementById("weatherform")
const para=document.getElementById("warningp")
function getweatherbycity(latitude,longitude){
    return function getweather(){
        fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
            .then((weather) => weather.json())
            .then((data) => temperaturespan.innerHTML=(data.current.temperature_2m))
            .catch((error) => {
            console.log(error)
        }) 
    }
}
function getcurrenttemperature(){
    if ('geolocation' in navigator){
        navigator.geolocation.getCurrentPosition((position) => {
            const getweatherfunction = getweatherbycity(position.coords.latitude,position.coords.longitude)
            getweatherfunction()
        });
    }
        else {
            para.innerHTML="Geolocation is not available"
        }
}
weatherform.addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent form submission
    getweatherbycity(latitude.value, longitude.value)();
})
trivandrum.addEventListener('click', getweatherbycity(10.3070,76.3341))
kottayam.addEventListener('click', getweatherbycity(9.5916,76.5222))
currentlocation.addEventListener('click', getcurrenttemperature)